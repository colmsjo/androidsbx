#!/bin/bash

STATUS=`xmllint -xpath "//*[local-name()='status']/text()" tmp.auth.xml`
TRANS_ID=`xmllint -xpath "//*[local-name()='transactionId']/text()" tmp.auth.xml`
ORDER_REF=`xmllint -xpath "//*[local-name()='orderRef']/text()" tmp.auth.xml`
AUTO_START=`xmllint -xpath "//*[local-name()='AutoStartToken']/text()" tmp.auth.xml`

echo -e "Input from auth\nStatus $STATUS, Transaction id: $TRANS_ID, Order ref: $ORDER_REF, Auto start token: $AUTO_START\n"

curl -sS \
--header 'Content-Type: text/xml;charset=UTF-8' \
--header 'SOAPAction:"urn:riv:Hemleverans:bankidCollectResponder:1:bankidCollect"' \
-d @collect.xml \
http://85.24.231.180:8000/bankidCollectResponder |tr -d '\n\t' > tmp.collect_result.xml

STATUS=`xmllint -xpath "//*[local-name()='status']/text()" tmp.collect_result.xml`
READY=`xmllint -xpath "//*[local-name()='ready']/text()" tmp.collect_result.xml`
SAML=`xmllint -xpath "//*[local-name()='SAMLReferens']/text()" tmp.collect_result.xml`
PNR=`xmllint -xpath "//*[local-name()='pnr']/text()" tmp.collect_result.xml`
FORNAMN=`xmllint -xpath "//*[local-name()='fornamn']/text()" tmp.collect_result.xml`
EFTERNAMN=`xmllint -xpath "//*[local-name()='efternamn']/text()" tmp.collect_result.xml`
AKT_AVTAL=`xmllint -xpath "//*[local-name()='aktuelltAvtalVersion']/text()" tmp.collect_result.xml`

echo -e "Result\nStatus $STATUS, Ready: $READY, SAML: $SAML, pnr: $PNR, fornamn: $FORNAMN, efternamn: $EFTERNAMN, aktuellt avtal: $AKT_AVTAL\n"
