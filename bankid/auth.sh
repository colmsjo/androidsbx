#!/bin/bash

curl -sS \
--header 'Content-Type: text/xml;charset=UTF-8' \
--header 'SOAPAction:"urn:riv:Hemleverans:bankidAuthenticateResponder:1:bankidAuthenticate"' \
-d @bankid_authenticate_request2.xml \
http://85.24.231.180:8000/bankidAuthenticateResponder |tr -d '\n\t' > tmp.auth.xml

STATUS=`xmllint -xpath "//*[local-name()='status']/text()" tmp.auth.xml`
TRANS_ID=`xmllint -xpath "//*[local-name()='transactionId']/text()" tmp.auth.xml`
ORDER_REF=`xmllint -xpath "//*[local-name()='orderRef']/text()" tmp.auth.xml`
AUTO_START=`xmllint -xpath "//*[local-name()='AutoStartToken']/text()" tmp.auth.xml`

echo -e "Status $STATUS, Transaction id: $TRANS_ID, Order ref: $ORDER_REF, Auto start token: $AUTO_START\n"

cat > ./collect.xml <<EOF
<?xml version="1.0" encoding="UTF-8" ?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:riv:Hemleverans:bankidCollectResponder:1:rivtabp21">
   <SOAP-ENV:Header/>
   <SOAP-ENV:Body>
           <bankidCollectRequest>
                   <transactionId>$TRANS_ID</transactionId>
                   <orderRef>$ORDER_REF</orderRef>
           </bankidCollectRequest>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
EOF
