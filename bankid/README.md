Perform SOAP Requests using cURL
================================

Bankid
-----

The scripts `auth.sh` and `collect.sh` performs a bankid authentication:

1. Run: `auth.sh`
2. Open the bankid app on the phone and enter the pin code
3. Run: `collect.sh` will fetch the SAML token


Initiate BankId login without pnr:

curl -sS \
--header 'Content-Type: text/xml;charset=UTF-8' \
--header 'SOAPAction:"urn:riv:Hemleverans:bankidAuthenticateResponder:1:bankidAuthenticate"' \
-d @bankid_authenticate_request.xml \
http://85.24.231.180:8000/bankidAuthenticateResponder -vvv


Initiate BankId login with pnr:

curl -sS \
--header 'Content-Type: text/xml;charset=UTF-8' \
--header 'SOAPAction:"urn:riv:Hemleverans:bankidAuthenticateResponder:1:bankidAuthenticate"' \
-d @bankid_authenticate_request2.xml \
http://85.24.231.180:8000/bankidAuthenticateResponder |tr -d '\n\t' |xml2json |json -H -2


Collect bankid authentication response:

curl -sS \
--header 'Content-Type: text/xml;charset=UTF-8' \
--header 'SOAPAction:"urn:riv:Hemleverans:bankidCollectResponder:1:bankidCollect"' \
-d @bankid_collect_response.xml \
http://85.24.231.180:8000/bankidCollectResponder -vvv


Sandbox
--------

curl -sS \
--header 'Content-Type: text/xml;charset=UTF-8' \
--header 'SOAPAction:"urn:riv:Hemleverans:bankidAuthenticateResponder:1:bankidAuthenticate"' \
-d @data.xml \
http://85.24.231.180:8000/bankidAuthenticateResponder -vvv
