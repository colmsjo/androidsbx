package runclass.soap_client;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import easy_wsdl.bankid.DIWbankidAuthenticateResponderBinding;
import easy_wsdl.bankid.DIWbankidAuthenticateResponseType;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // from easywsdl sample
        DIWbankidAuthenticateResponderBinding service=new DIWbankidAuthenticateResponderBinding(null,"http://service.address.url");

        DIWbankidAuthenticateResponseType clientInfo = new DIWbankidAuthenticateResponseType();
        clientInfo.ClientInstanceId= UUID.randomUUID();
        clientInfo.ApplicationLanguage="pl";
        clientInfo.ApplicationVersion="1.0.0.0";
        clientInfo.Version="4.5.0.0";
        clientInfo.Platform= Enums.PlatformType.Android;
        clientInfo.PlatformVersion="Android Test app";

        SessionData sessionData= service.Login(clientInfo,"username","password");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
