package com.gizur.guisbx2;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    TextView t00, t01, t10, t11, t20, t21, t30, t31, t40, t41;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        t00 = (TextView) findViewById(R.id.text00);
        t01 = (TextView) findViewById(R.id.text01);

        t10 = (TextView) findViewById(R.id.text10);
        t11 = (TextView) findViewById(R.id.text11);

        t20 = (TextView) findViewById(R.id.text20);
        t21 = (TextView) findViewById(R.id.text21);

        t30 = (TextView) findViewById(R.id.text30);
        t31 = (TextView) findViewById(R.id.text31);

        t40 = (TextView) findViewById(R.id.text40);
        t41 = (TextView) findViewById(R.id.text41);

        t00.setClickable(true);
        t01.setClickable(true);
        t10.setClickable(true);
        t11.setClickable(true);
        t20.setClickable(true);
        t21.setClickable(true);
        t30.setClickable(true);
        t31.setClickable(true);
        t40.setClickable(true);
        t41.setClickable(true);

        t00.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), "[0,0] Clicked",
                        Toast.LENGTH_SHORT).show();
            }
        });

        t01.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), "[0,1] Clicked",
                        Toast.LENGTH_SHORT).show();
            }
        });

        t10.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), "[1,0] Clicked",
                        Toast.LENGTH_SHORT).show();
            }
        });

        t11.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), "[1,1] Clicked",
                        Toast.LENGTH_SHORT).show();
            }
        });

        t20.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), "[2,0] Clicked",
                        Toast.LENGTH_SHORT).show();
            }
        });

        t21.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), "[2,1] Clicked",
                        Toast.LENGTH_SHORT).show();
            }
        });

        t30.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), "[3,0] Clicked",
                        Toast.LENGTH_SHORT).show();
            }
        });

        t31.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), "[3,1] Clicked",
                        Toast.LENGTH_SHORT).show();
            }
        });

        t40.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), "[3,0] Clicked",
                        Toast.LENGTH_SHORT).show();
            }
        });

        t41.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                Toast.makeText(getBaseContext(), "[3,1] Clicked",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
}