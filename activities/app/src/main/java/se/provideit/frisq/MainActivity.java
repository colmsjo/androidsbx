/**
 * @author Jonas Colmsjö, ProvideIT
 *
 * Taking inspiration from theese code styles (but not adhering not stricly to them):
 *  + https://google.github.io/styleguide/javaguide.html
 *  + https://source.android.com/source/code-style.html
 *
 */

package se.provideit.frisq;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;
import android.content.Intent;

public class MainActivity extends Activity {

    ArrayList<View> tt = new ArrayList<View>();
    int[] ids = {R.id.faq_icon, R.id.fullmakter_icon, R.id.logga_in_icon, R.id.meddelanden_icon,
                R.id.mina_uppgifter_icon, R.id.recept_icon};

    HashMap<Integer, Integer> btnToLayout = new HashMap<Integer, Integer>();
    HashMap<Integer, Class> btnToClass = new HashMap<Integer, Class>();

    private void handleClick(int id) {
        int layoutId = btnToLayout.get(id);
        Class activityClass = btnToClass.get(id);

        String msg = "Clicked on: " + id + ", openning layout: " + layoutId;
        System.out.println(msg);
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, activityClass);
        View view = (View) findViewById(layoutId);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // setup layouts and classes that is used in the navigation
        btnToLayout.put(R.id.faq_icon, R.layout.activity_faq);
        btnToClass.put(R.id.faq_icon, FaqActivity.class);

        btnToLayout.put(R.id.fullmakter_icon, R.layout.activity_faq);

        // show start screen
        setContentView(R.layout.linear);

        // setup event listeners
        for(int id: ids) {
            final View t = (View) findViewById(id);
            final int id2 = id;
            tt.add(t);
            t.setClickable(true);

            t.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    handleClick(id2);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
}