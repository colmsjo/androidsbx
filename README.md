My notes
========


Activities are single things that users can do. Typically some user interaction is performed.

The lifecycle has four states:

1. active/running - e.g. in the foreground
2. lost focus but visible
3. stopped - not visible
4. killed



Link: http://developer.android.com/reference/android/app/Activity.html

