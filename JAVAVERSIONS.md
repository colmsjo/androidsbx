Dalvik / ART (Android Runtime)
-----------------------------

Should be possible to use lamdas in Android, see:
http://tools.android.com/tech-docs/new-build-system/user-guide#TOC-Using-sourceCompatibility-1.7


Closures in 1.7
---------------

http://www.javabeat.net/closures-in-java-7-0/


Manage Java versions
---------------------

List all installed versions: `/usr/libexec/java_home -verbose`

bash_profile uses the latest installed version: `export JAVA_HOME=$(/usr/libexec/java_home)`

For instance, use 1.7 instead: `JAVA_HOME=$(/usr/libexec/java_home -v '1.7*')`


REPL
----

Java REPL: https://github.com/albertlatacz/java-repl


Language history
----------------

 * 1.8 - lambda expressions
 * 1.7 - support for dynamic languages (invocedynamic)
 * 1.5 - Annotations, Generics
 * 1.4 - assert, NIO



Functional programming
---------------------

Java 1.8 has lambda expressions.

There is also: http://www.functionaljava.org


