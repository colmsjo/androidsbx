package se.provideit.frisq;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    ArrayList<View> tt = new ArrayList<View>();
    int[] ids = {R.id.faq_icon, R.id.fullmakter_icon, R.id.logga_in_icon, R.id.meddelanden_icon,
                R.id.mina_uppgifter_icon, R.id.recept_icon};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.linear);

        for(int id: ids) {
            final View t = (View) findViewById(id);
            tt.add(t);
            t.setClickable(true);

            t.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    Toast.makeText(getBaseContext(), t + " Clicked",
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
}