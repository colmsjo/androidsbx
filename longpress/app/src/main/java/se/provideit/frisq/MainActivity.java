package se.provideit.frisq;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import android.support.v4.view.GestureDetectorCompat;

public class MainActivity extends Activity  {

    ArrayList<View> tt = new ArrayList<View>();
    int[] ids = {R.id.faq_icon, R.id.fullmakter_icon,  R.id.meddelanden_icon,
                R.id.mina_uppgifter_icon, R.id.recept_icon};

    GestureDetectorCompat detector;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("START", "onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.linear);

        detector = new GestureDetectorCompat(this, new MyGestureListener());

        for(int id: ids) {
            final View t = (View) findViewById(id);
            tt.add(t);
            t.setClickable(true);

            t.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    Log.d("START", "onClick: " + t);
                    Toast.makeText(getBaseContext(), t + " Clicked",
                            Toast.LENGTH_SHORT).show();
                }
            });

            t.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    Log.d("START", "onLongPress:" + t + new java.util.Date());
                    Toast.makeText(getBaseContext(), "onLongPress:" + t + new java.util.Date(),
                            Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.detector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public void onLongPress(MotionEvent event) {
            Log.d("START", "onLongPress in activity:" + new java.util.Date());
            Toast.makeText(getBaseContext(), "onLongPress in activity:" + new java.util.Date(),
                    Toast.LENGTH_SHORT).show();
        }

    }
}